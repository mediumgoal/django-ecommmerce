
from django.urls import path, include
from django.views.generic import TemplateView

app_name = "search"
from .views import SearchProductView

urlpatterns = [
    path('', SearchProductView.as_view(), name="query"),

] 
