from django import forms
from .models import Adress

class AdressForm(forms.ModelForm):
    class Meta:
        model = Adress
        fields = [
            'adress_line_1', 
            'adress_line_2', 
            'city',
            'country',
            'state', 
            'postal_code'
        ]