from django.shortcuts import render, redirect
from .forms import AdressForm
from django.utils.http import is_safe_url
from billing.models import BillingProfile
from .models import Adress

def checkout_address_create_view(request):
    form = AdressForm(request.POST or None)
    context = {
        "form":form
    }
    next_ = request.GET.get('next')
    next_post = request.POST.get('next')
    redirect_path = next_ or next_post or None
    if form.is_valid():
        print(request.POST)
        instance = form.save(commit=False)
        billing_profile, billing_profile_created = BillingProfile.objects.new_or_get(request)
        if billing_profile is not None:
            adress_type = request.POST.get('address_type', 'shipping')
            instance.billing_profile = billing_profile
            instance.adress_type = request.POST.get('address_type', 'shipping')
            instance.save()

            request.session[adress_type + "_adress_id"] = instance.id
            print(adress_type + "_adress_id")
        else:
            print("Error, here")
            return redirect("cart:checkout")
        if is_safe_url(redirect_path, request.get_host()):
            return redirect(redirect_path)
        else:
            redirect('cart:checkout')
    return redirect('cart:checkout')

def checkout_address_reuse_view(request):
    if request.user.is_authenticated:
        next_ = request.GET.get('next')
        next_post = request.POST.get('next')
        redirect_path = next_ or next_post or None
        if request.method == "POST":
            print(request.POST)
            shipping_adress = request.POST.get("shipping_adress", None)
            adress_type = request.POST.get('address_type', 'shipping')
            billing_profile, billing_profile_created = BillingProfile.objects.new_or_get(request)
            if shipping_adress is not None:
                qs = Adress.objects.filter(billing_profile=billing_profile, id=shipping_adress)
                if qs.exists():
                    request.session[adress_type + "_adress_id"] = shipping_adress

                if is_safe_url(redirect_path, request.get_host()):
                    return redirect(redirect_path)
    return redirect('carts:checkout')
