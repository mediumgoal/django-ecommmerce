from django.db import models
from billing.models import BillingProfile

ADRESS_TYPES = (
    ('billing', 'Billing'),
    ('shipping', "Shipping"),
)


class Adress(models.Model):
    billing_profile = models.ForeignKey(BillingProfile, on_delete = models.CASCADE)
    adress_type     = models.CharField(max_length=120, choices=ADRESS_TYPES)
    adress_line_1   = models.CharField(max_length=120)
    adress_line_2   = models.CharField(max_length=120, blank=True, null=True)
    city            = models.CharField(max_length=120)
    country         = models.CharField(max_length=120, default="Turkey")
    state           = models.CharField(max_length=120)
    postal_code     = models.CharField(max_length=120)

    def __str__(self):
        return str(self.billing_profile)
    
    def get_adress(self):
        return "{}\n{}\n{}\n{}{}\n{}".format(
            self.adress_line_1,
            self.adress_line_2 or "",
            self.city,
            self.state,
            self.postal_code,
            self.country
        )

