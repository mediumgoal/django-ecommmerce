from django.shortcuts import render, redirect
from .forms import LoginForm, RegisterForm, GuestForm
from django.contrib.auth import authenticate, login, get_user_model, logout
from django.utils.http import is_safe_url
from .models import GuestModel
from .signals import user_logged_in



def guest_register(request):
    login_form = GuestForm(request.POST or None)
    context = {
        "form":login_form
    }

    next_ = request.GET.get('next')
    next_post = request.POST.get('next')
    redirect_path = next_ or next_post or None

    if login_form.is_valid():
        email = login_form.cleaned_data.get("email")
        new_guest_email = GuestModel.objects.create(email=email)
        request.session["guest_email_id"] = new_guest_email.id
        request.session["guest_email"] = new_guest_email.email

        if is_safe_url(redirect_path, request.get_host()):
            return redirect(redirect_path)
        else:
            redirect('/register')
   
    return redirect("/register")




def login_page(request):
    login_form = LoginForm(request.POST or None)
    context = {
        "form":login_form
    }

    next_ = request.GET.get('next')
    next_post = request.POST.get('next')
    redirect_path = next_ or next_post or None

    if login_form.is_valid():
        #print(login_form.cleaned_data)
        username = login_form.cleaned_data.get("username")
        password = login_form.cleaned_data.get("password")
        user = authenticate(username=username, password=password)
        #print(request.user.is_authenticated)
        if user is not None:
            #print(request.user.is_authenticated)
            login(request, user)
            user_logged_in.send(user.__class__, instance = user,request = request)
            try:
                del request.session["guest_email_id"]
            except :
                pass
                
            if is_safe_url(redirect_path, request.get_host()):
                return redirect(redirect_path)
            else:
                redirect('/')
        else:
           pass
       
   
    return render(request, "accounts/login.html", context)

User = get_user_model()
def register_page(request):
    register_form = RegisterForm(request.POST or None)
    context = {
        "form":register_form
    }
    if register_form.is_valid():
        print(register_form.cleaned_data)
        username = register_form.cleaned_data.get("username")
        password = register_form.cleaned_data.get("password")
        email = register_form.cleaned_data.get("email")
        new_user =  User.objects.create_user(username, email, password)
        print(new_user)
    return render(request, "accounts/register.html", context)

def logout_page(request):
    logout(request)
    if request.user.is_authenticated:
        print("user out")
    else:
        print("user not out")
    return redirect('login')