from django.http import JsonResponse
from django.shortcuts import render, redirect
from products.models import Product
from orders.models import Order
from .models import Cart
from accounts.forms import LoginForm, GuestForm
from adresses.forms import AdressForm
from adresses.models import Adress
from billing.models import BillingProfile
from accounts.models import GuestModel



def cart_detail_api_view(request):
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    products = [{
            "id":x.id,
            "url":x.get_absolute_url(),
            "name":x.name, 
            "price":x.price
        } 
        for x in cart_obj.products.all()
    ] # [<object>, <object>]
    cart_data = {
        "products":products, 
        "subtotal":cart_obj.subtotal, 
        "total": cart_obj.total
    }
    return JsonResponse(cart_data)



def cart_home(request):
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    context = {"cart":cart_obj}
    return render(request, "carts/home.html", context)

def cart_update(request):
    product_id = request.POST.get("product_id")
    if product_id is not None:
        try:  
            product_obj = Product.objects.get(id=product_id)
        except ProductDoesNotExist:
            print("show me message to user, product has gone")
            return redirect("home")
        cart_obj, new_obj = Cart.objects.new_or_get(request)
        if product_obj in cart_obj.products.all():
            cart_obj.products.remove(product_obj)
            added = False
        else:
            added = True
            cart_obj.products.add(product_obj)
        request.session["cart_items"] = cart_obj.products.count()

        if request.is_ajax():
            print("ajax request")
            json_data = {
                "added":added,
                "removed":not added,
                "cartItemCount":cart_obj.products.count(),

            }
            return JsonResponse(json_data)
            #return JsonResponse({"message":"error 400"}, status_code=400) # Django rest framework
    return redirect("carts:home")

def checkout_home(request):
    cart_obj, cart_created = Cart.objects.new_or_get(request)
    order_obj = None
    if cart_created or cart_obj.products.count() == 0: #or request.user.is_authenticated == False:
        return redirect("carts:home")

    login_form = LoginForm()
    guest_form = GuestForm()
    address_form = AdressForm()

    billing_adress_id = request.session.get("billing_adress_id", None)
    shipping_adress_id = request.session.get("shipping_adress_id", None)


    billing_profile, billing_profile_created = BillingProfile.objects.new_or_get(request)
    adress_qs = None
    if billing_profile is not None:
        if request.user.is_authenticated:
            adress_qs = Adress.objects.filter(billing_profile = billing_profile)
        order_obj, order_obj_created = Order.objects.new_or_get(billing_profile, cart_obj)
        if shipping_adress_id:
            order_obj.shipping_adress = Adress.objects.get(id=shipping_adress_id)
            del request.session["shipping_adress_id"]
        elif billing_adress_id:
            order_obj.billing_adress = Adress.objects.get(id=billing_adress_id)
            del request.session["billing_adress_id"]
        if billing_adress_id or shipping_adress_id:
            order_obj.save()
    
    if request.method == "POST":
        "check that order is done"
        is_done = order_obj.check_done()
        if is_done:
            order_obj.mark_paid()
            del request.session["cart_id"]
            request.session["cart_items"] = 0
            return redirect("carts:success")



    context = {
        "object":order_obj,
        "billing_profile":billing_profile,
        "login_form":login_form,
        "guest_form":guest_form,
        "address_form":address_form,
        "adress_qs":adress_qs
    }
    return render(request, "carts/checkout.html", context)


def checkout_done(request):
    return render(request, "carts/checkout-done.html")