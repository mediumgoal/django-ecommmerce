
from django.urls import path

from .views import (
    ProductListView, 
    #product_list_view, 
    #ProductDetailView, 
    #product_detail_view,
    #ProductFeaturedDetailView,
    ProductSlugDetailView,
    #ProductFeaturedListView
    )

app_name = "products"

urlpatterns = [

    path('', ProductListView.as_view(), name = "list"),
    #path('featured/<int:pk>', ProductFeaturedDetailView.as_view()),
    #path('products-fbv/<slug:slug>', product_detail_view, name = "detail"),
    #path('products/<int:pk>', ProductDetailView.as_view()),
    path('<slug:slug>', ProductSlugDetailView.as_view(), name = "detail"),

] 
