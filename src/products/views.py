from django.http import Http404
from django.views.generic import ListView, DetailView
from django.shortcuts import render, get_object_or_404

from .models import Product
from carts.models import Cart

from analytics.signals import object_viewed_signal
from analytics.mixin import ObjectViewedMixin


class ProductFeaturedListView(ListView):
    template_name = "products/list.html"
    def get_queryset(self, *args, **kwargs):
        request = self.request
        return Product.objects.featured()
    
class ProductFeaturedDetailView(DetailView, ObjectViewedMixin):
    template_name = "products/featured-detail.html"
    def get_queryset(self, *args, **kwargs):
        request = self.request
        return Product.objects.featured()

class ProductListView(ListView):
    queryset = Product.objects.all()
    template_name = "products/list.html"

    def get_context_data(self, *args, **kwargs):
        context = super(ProductListView, self).get_context_data(*args, **kwargs)
        request = self.request
        cart_obj, new_obj = Cart.objects.new_or_get(request)
        context["cart"] = cart_obj
        return context

    #def get_context_data(self, ***args, **kwargs):
       # context = super(ProductListView, self).get_context_data(*args, **kwargs)
       # print(context)
       # return context

def product_list_view(request):
    queryset = Product.objects.all()
    context = {
        "object_list" : queryset
    }
    return render(request, "products/list.html", context)


class ProductSlugDetailView(DetailView):
    queryset = Product.objects.all()
    ObjectViewedMixin()
    template_name = "products/detail.html"
    
    def get_context_data(self, *args, **kwargs):
        context = super(ProductSlugDetailView, self).get_context_data(*args, **kwargs)
        request = self.request
        cart_obj, new_obj = Cart.objects.new_or_get(request)
        context["cart"] = cart_obj
        return context

    def get_object(self, *args, **kwargs):
        request = self.request
        slug = self.kwargs.get('slug')
        
        try:
            instance = Product.objects.get(slug=slug, active = True)
        except Product.DoesNotExist:
            raise Http404("Böyle bir ürün yok")
        except Product.MultipleObjectsReturned:
            qs = Product.objects.filter(slug = slug, active = True)
            queryset =  qs.first()
        except: 
            raise Http404("huh?")

        object_viewed_signal.send(instance.__class__, instance = instance, request=request)
        return instance

    

class ProductDetailView(DetailView, ObjectViewedMixin):
    queryset = Product.objects.all()
    template_name = "products/detail.html"
    
    #def get_context_data(self, ***args, **kwargs):
       # context = super(ProductListView, self).get_context_data(*args, **kwargs)
       # print(context)
       # return context

def product_detail_view(request, slug = None, *args, **kwargs):
    #print(pk)
    #instance = get_object_or_404(Product, pk = pk)

    qs = Product.objects.filter(slug = slug)
    if qs.exists() and qs.count() == 1:
        instance = qs.first()
        tags = instance.tag_set.all()
    else:
        raise Http404("Böyle bir ürün yok!")
    context = {
        "object" : instance,
        "tags":tags
    }
    return render(request, "products/detail.html", context)