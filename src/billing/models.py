from django.conf import settings
from django.db import models
from django.db.models.signals import post_save, pre_save
from accounts.models import GuestModel


User = settings.AUTH_USER_MODEL

import stripe
stripe.api_key = "sk_test_1MmZQpivkOQTi5YJk26Go7Ut00xK8kvf0Q"

class BillingProfileManager(models.Manager):
    def new_or_get(self, request):
        user = request.user
        guest_email_id = request.session.get("guest_email_id")
        created = False
        obj = None
        if user.is_authenticated:
            obj, created = self.model.objects.get_or_create(user=user, email=user.email)

        elif guest_email_id is not None:
            guest_email = request.session.get("guest_email")
            geust_email_obj = GuestModel.objects.get(id=guest_email_id)
            obj, vreated = self.model.objects.get_or_create(email=guest_email)

        else:
            pass
        return obj, created

class BillingProfile(models.Model):
    user        = models.OneToOneField(User, blank=True, null=True, on_delete = models.CASCADE)
    email       = models.EmailField()
    timestamp   = models.DateTimeField(auto_now_add=True)   
    update      = models.DateTimeField(auto_now_add=True)
    active      = models.BooleanField(default=True)
    customer_id = models.CharField(max_length = 120, blank=True, null=True)

    objects = BillingProfileManager()

    def __str__(self):
        return self.email

def billing_profile_created_reciver(sender, instance, *args, **kwargs):
    if not instance.customer_id and instance.email:
        print("Send to stripe")
        customer = stripe.Customer.create(
            email = instance.email
        )
        print(customer)
        instance.customer_id = customer.id
        instance.save


pre_save.connect(billing_profile_created_reciver, sender=BillingProfile)


def user_created_reciever(sender, instance, created, *args, **kwargs):
     if created:
         BillingProfile.objects.get_or_create(user=instance, email=instance.email)

post_save.connect(user_created_reciever, sender=User)