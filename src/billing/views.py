from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from django.utils.http import is_safe_url

import stripe
stripe.api_key = "sk_test_1MmZQpivkOQTi5YJk26Go7Ut00xK8kvf0Q"
STRIPE_PUB_KEY = "pk_test_riK2fmuBuzi8CxwNab83OtY100juhPexLw"

def payment_method_view(request):
    next_ = request.GET.get('next')
    next_url = None
    if is_safe_url(next_, request.get_host()):
        next_url = next_
    return render(request,"billing/payment-method.html",{"publish_key":STRIPE_PUB_KEY, "next_url":next_url})

def payment_method_create_view(request):
    if request.method == "POST":
        data = {
            "message":"success",

        }
        return JsonResponse(data)
    return HttpResponse("Error", status_code=401)