from django.db import models
from django.db.models.signals import pre_save, post_save
from ecommerce.utils import unique_order_id_generator
from carts.models import Cart
from billing.models import BillingProfile
from adresses.models import Adress

import math

ORDER_STATUS_CHOICES = (
    ('created', 'Created'), 
    ('paid', 'Paid'),
    ('shipped', 'Shipped'),
    ('refunded', 'Refunded')
)


class OrderManager(models.Manager):
    def new_or_get(self, billing_profile, cart_obj):
        qs = self.get_queryset().filter(cart=cart_obj, billing_profile = billing_profile, active=True, status="created")
        if qs.count() == 1:
            created= False
            obj = qs.first()
        else:
            created= True
            obj = self.model.objects.create(cart=cart_obj, billing_profile = billing_profile)
        return obj, created


class Order(models.Model):
    order_id        = models.CharField(max_length=120, blank = True) ##random and unique
    billing_profile = models.ForeignKey(BillingProfile, on_delete = models.CASCADE, blank=True, null=True)
    shipping_adress = models.ForeignKey(Adress, related_name = "shipping_adress", on_delete = models.CASCADE, blank=True, null=True)
    billing_adress  = models.ForeignKey(Adress, related_name = "billing_adress", on_delete = models.CASCADE, blank=True, null=True)
    cart            = models.ForeignKey(Cart, on_delete=models.CASCADE)
    status          = models.CharField(max_length=120,default='created', choices=ORDER_STATUS_CHOICES)
    shipping_total  = models.DecimalField(default=5.99, max_digits=100, decimal_places=2)
    total           = models.DecimalField(default=0.00, max_digits=100, decimal_places=2)
    active          = models.BooleanField(default=True)
    
    objects =  OrderManager()

    def check_done(self):
        billing_profile = self.billing_profile
        shipping_adress = self.shipping_adress
        billing_adress = self.billing_adress
        total = self.total
        if billing_profile and shipping_adress and billing_adress and total > 0:
            return True
        return False
    
    def mark_paid(self):
        if self.check_done():
            self.status = 'paid'
            self.save()
        return self.status
    
    def __str__(self):
        return self.order_id

    def update_total(self):
        cart_total = self.cart.total
        shipping_total = self.shipping_total
        new_total = math.fsum([cart_total, shipping_total])
        self.total =  new_total
        self.save()
        return new_total

##generate order_id
##generate order_total

def pre_save_create_order_id(sender, instance, *args, **kwargs):
    if not instance.order_id:
        instance.order_id = unique_order_id_generator(instance)
    qs = Order.objects.filter(cart=instance.cart).exclude(billing_profile = instance.billing_profile)
    if qs.exists():
        qs.update(active=False)

pre_save.connect(pre_save_create_order_id, sender=Order )

def post_save_cart_total(sender, instance, created,*args, **kwargs):
    if not created:
        cart_obj = instance
        cart_total = cart_obj.total
        cart_id = cart_obj.id
        qs = Order.objects.filter(cart__id=cart_id)
        if qs.count() == 1 :
            order_obj = qs.first()
            order_obj.update_total()

post_save.connect(post_save_cart_total, sender=Cart)

def post_save_order(sender, instance, created,*args, **kwargs):
    print("running")
    if created:
        print("updating... first")
        instance.update_total()

post_save.connect(post_save_order, sender=Order)