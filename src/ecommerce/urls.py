"""ecommerce URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls.static import static


from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView

from django.contrib.auth import logout


from adresses.views import checkout_address_create_view, checkout_address_reuse_view
from carts.views import cart_home, cart_detail_api_view
from accounts.views import login_page, register_page, logout_page, guest_register
from billing.views import payment_method_view, payment_method_create_view
from . import views
#from products.views import (
    #ProductListView, 
    #product_list_view, 
    #ProductDetailView, 
    #product_detail_view,
    #ProductFeaturedDetailView,
    #ProductSlugDetailView,
    #ProductFeaturedListView
    #)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home_page, name = "home"),
    path('contact/', views.contact_page, name = "contact"),
    path('about/', views.about_page),
    path('products/', include("products.urls", namespace="products")),
    path('search/',include("search.urls", namespace="search")),
    path('cart/', include("carts.urls", namespace="carts")),
    path('api/cart/', cart_detail_api_view, name = "api-cart"),
    path('checkout/address/create/', checkout_address_create_view, name = "checkout_address_create"),
    path('checkout/address/reuse/', checkout_address_reuse_view, name = "checkout_address_reuse"),
    path('login/', login_page, name = "login"),
    path('register/guest', guest_register, name = "guest_register"),
    path('billing/payment-method', payment_method_view, name = "billing-payment-method"),
    path('billing/payment-method/create', payment_method_create_view, name = "billing-payment-method-endpoint"),
    path('logout/', logout_page, name = "logout"),
    path('bootstrap/', TemplateView.as_view(template_name = "bootstrap/exanple.html"), name = "bootstrap"),
    path('register/', register_page, name = "register")
] 

if settings.DEBUG:
    urlpatterns = urlpatterns + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns = urlpatterns + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)