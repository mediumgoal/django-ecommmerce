from django.shortcuts import render, HttpResponse, redirect
from .forms import ContactForm
from django.contrib.auth import authenticate, login, get_user_model
from django.http import JsonResponse


def home_page(request):
    print(request.session.get('first_name','Bilinmiyor'))
    context = {
        "title":"hellow world!",
        "data":"Merhaba ecommerce siteme hoş geldiniz",
    }
    if request.user.is_authenticated:
        context["premium_content"] = "yeeeahh"
    return render(request, 'home_page.html', context)

def about_page(request):
    context = {
        "title":"about page",
        "data":"Ümit bisiklet 2019 yılında kar marjını yükselterek bu günlere gelmiş tek bisiklet markasıdır"
    }
    return render(request, 'home_page.html', context)

def contact_page(request):
    contact_form = ContactForm(request.POST or None)
    if contact_form.is_valid():
        print(contact_form.cleaned_data)
        if request.is_ajax():
            return JsonResponse({"message":"thank you!"})
    context = {
        "title":"cotnact page",
        "data":"İletişim için lütfen arayın: 0548 487 87 87",
        "form":contact_form
    }
    return render(request, 'contact/view.html', context) 

