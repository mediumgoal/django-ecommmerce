$(document).ready(function(){
    //contact form handler
    var contactForm = $(".contact-form");
    var contactFormMethod = contactForm.attr("method");
    var contaactFormAction = contactForm.attr("action");
    contactForm.submit(function(e){
      e.preventDefault();
      var contactFormData = contactForm.serialize();
      var thisForm = $(this);
      $.ajax({
        method: contactFormMethod,
        url: contaactFormAction,
        data: contactFormData,
        success: function(data){
          thisForm[0].reset();
          alert("Teşekkürler iletiniz gönderildi", " mesaj: " + data.message);
        },
        error: function(errorData){
          alert("Hata meydana geldi");
          console.log(errorData);
        }
      });
    });














    //auto search
    var searchForm = $("form.search-form");
    var searchInput = searchForm.find("[name='q']");
    var typingTimer;
    var typingInterval = 1500 // .5 saniye
    var searchBtn = searchForm.find("[type=submit]");

    searchInput.keyup(function(event){
      clearTimeout(typingTimer);
      typingTimer = setTimeout(performSearch, typingInterval);
    });

    searchInput.keydown(function(event){
      clearTimeout(typingTimer);
    });

    function doSearch(){
      searchBtn.addClass('disabled');
      searchBtn.html('<i class = "fa fa-spin fa-spinner"></i> Searching');
    }

    function performSearch(){
      doSearch();
      var query = searchInput.val();
      setTimeout(function(){
        window.location.href = '/search/?q=' + query;
      }, 1000)
      
    }

    



    //cart add products
    var productForm = $(".form-product-ajax");
    productForm.submit(function(event){
      event.preventDefault();
      console.log("form is not sending right now");
      var thisForm = $(this);
      //var actionEndpoint = thisForm.attr("action");
      var actionEndpoint = thisForm.attr("data-endpoint");
      var httpMethod = thisForm.attr("method");
      var formData = thisForm.serialize();

      console.log(formData)
      $.ajax({
        url: actionEndpoint,
        method: httpMethod,
        data: formData,
        success: function(data){
          var submitSpan = thisForm.find(".submit-span");

          if(data.added){
            submitSpan.html('in  cart <button type="submit" class="btn btn-link">Remove?</button>');
          }else{
            submitSpan.html('<button type="submit" class="btn btn-success">Add to cart</button>');
          }
          var navbarCount = $(".navbar-cart-count");
          navbarCount.text(data.cartItemCount);

          var currentPath = window.location.href;
          if(currentPath.indexOf("cart") != -1){
            refreshCart();
          }
        },
        error: function(errorData){
          alert("and error occured");
          console.log("error: ",errorData);
        }
      });
    })

    function refreshCart(){
      var cartTable = $(".cart-home");
      var cartBody = cartTable.find(".cart-body");
      //cartBody.html("<h1>changed</h1>");
      var productRows = cartBody.find(".cart-products");
      var currentUrl = window.location.href;
      var refreshCartUrl = "/api/cart";
      var refreshCartMethod = "GET";
      var data = {};
      $.ajax({
        url: refreshCartUrl,
        method: refreshCartMethod,
        data: data,
        success: function(data){

          var hiddenCartItemRemoveForm = $(".cart-item-remove-form");
          if(data.products.length > 0 ){
            $(".cart-products").html('');
            var loopcounter=1;
            $.each(data.products, function(index, value){
              console.log("value geliyor: ");
              var newCartItemRemove = hiddenCartItemRemoveForm.clone();
              newCartItemRemove.css("display","block");
              newCartItemRemove.find(".cart-product-id").val(value.id);
              cartBody.append("<tr><th scope=\"row\">" + loopcounter +"</th><td><a href = '"+ value.url+ "'>" + value.name + "</a></td><td>" + value.price + "</td><td>" + newCartItemRemove.html() + "</tr>");
              loopcounter++;
            });
            
          }else{
            window.location.href = currentPath;
          } 
          $(".cart-total").text(data.total);
          $(".cart-subtotal").text(data.subtotal);
          console.log("success api cart");
          console.log(data);
        },
        error: function(errorData){
          console.log("error", errorData)
        }
      });
    }

  });